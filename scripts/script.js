fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(res => {
        if (res.status === 200) {
            return res.json();
        } else {
            document.body.innerHTML = `<h1 style="color:red"> Missing! Error! </h1>`;
        }
    })
    .then(data => {

        data.forEach(({episodeId, name, openingCrawl, characters}) => {

            new StarWars(episodeId, name, openingCrawl, characters).render()
            console.log(characters)
        });
    })
    .catch(err => {
        console.log(err)
    })


class StarWars {
    constructor(episodeId, name, openingCrawl, characters) {
        this.episodeId = episodeId;
        this.name = name;
        this.openingCrawl = openingCrawl;
        this.characters = characters;
        this.container = document.querySelector('.container');
        this.act = document.createElement('ol')
    }

    fetchLi() {
        this.characters.forEach(url => {

            fetch(url)
            
                .then(res => res.json())
                .then(({ name: actor }) => {   

                    const li = document.createElement('li');
                    this.act.append(li);
                    li.innerHTML = actor;
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    createCard() {
        const card = `
        <ul class="card">
            <li> Епізод: ${this.episodeId}</li>
            <li> Назва фільму: ${this.name}</li>
            <li> Сюжет: ${this.openingCrawl}</li>
        </ul>
        `
        this.container.insertAdjacentHTML('beforeend', card);
        this.container.append(this.act)
    }

    render() {
        this.createCard();
        this.fetchLi() 
    }
}
